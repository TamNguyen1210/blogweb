﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IArticleRepository :IRepository
    {
        IList<Article> GetAllArticleByUserId(int UserId,bool IsAdmin);
        Article GetArticleBySlug(string Slug);
        IList<Comment> GetAllCommentByArticleId(int ArticleId);
        string GetSlugByArticleId(int ArticleId);
        int GetAmountCommentByArticleId(int ArticleId);
        int GetAmountArticleByUserId(int UserId, bool IsAdmin);
        Article GetRandomArticleExceptOne(Article _article);
    }
}
