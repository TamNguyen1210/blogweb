﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace Core.Interfaces
{
    public interface IUserRepository : IRepository
    {
        User GetCurrentUserByUserName(string UserName);
        User GetUserByUserName(string UserName);
        User CheckLogin(string UserName, string Password);
        int GetAmountArrticleByUserName(string UserName);
        int GetCurrentUserIDByUserName(string UserName);
    }   
}
