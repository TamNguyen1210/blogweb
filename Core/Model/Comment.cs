﻿using Core.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Model
{
    public class Comment : BaseModel<int>
    {                       
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        //[ForeignKey("User")]        
        public int UserId { get; set; }
        public virtual User User { get; set; }
        //[ForeignKey("Article")]
        public int ArticleId { get; set; }
        public virtual Article Article { get; set; }
    }
}