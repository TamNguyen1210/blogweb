﻿using BLL;
using Core.Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace UnitTesting.ArticleTest
{
    class GetSlugByArticleIdTest
    {
        public IArticleBUS MockArticleRepository;
        private readonly ArticleTest _article = new ArticleTest();
        

        public GetSlugByArticleIdTest()
        {
            MockArticleRepository = _article.mockArticleRepository.Object;
        }

        [Fact]
        public void GetSlugByArticleId_InvalidId_ReturnNull()
        {
            //string articleSlug = "Silverlight-Unleashed";
            string result = this.MockArticleRepository.GetSlugByArticleId(5);

            Assert.Null(result);
        }
    }
}
