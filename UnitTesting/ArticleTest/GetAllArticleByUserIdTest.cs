﻿using BLL;
using Core.Model;
using System.Collections.Generic;
using Xunit;

namespace UnitTesting.ArticleTest
{
    class GetAllArticleByUserIdTest
    {
        public IArticleBUS MockArticleRepository;
        private readonly ArticleTest _article = new ArticleTest();
        
        public GetAllArticleByUserIdTest()
        {           
            MockArticleRepository = _article.mockArticleRepository.Object;
        }

        [Fact]
        public void GetAllArticleByUserId_InvalidId_ReturnNull()
        {
            IList<Article> testArticle = MockArticleRepository.GetAllArticleByUserId(5, false);
            Assert.Null(testArticle);
        }

        [Fact]
        public void CanGetAllArticleByUserId()
        {
            IList<Article> testArticle = MockArticleRepository.GetAllArticleByUserId(1,false);
            
            Assert.NotNull(testArticle);            
            Assert.Equal(testArticle.Count, 1);
        }
      
    }
}
