﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using Core.Model;
using BLL;

namespace UnitTesting.ArticleTest
{
    public class ArticleTest
    {
        public Mock<IArticleBUS> mockArticleRepository { get; set; }
        public List<Article> articles { get; set; } 
        public ArticleTest()
        {
            mockArticleRepository = new Mock<IArticleBUS>();
            articles  = new List<Article>()
                {
                    new Article { Id = 1, Title = "C# Unleashed",
                        Content = "Short description here", Slug = "C#-Unleashed",
                        Owner = new User{ Id = 1, FullName = "User 1", UserName="user1",Password="123456", Role="Admin" } },
                    new Article { Id = 2, Title = "ASP.Net Unleashed",
                        Content = "Short description here", Slug = "ASP.Net-Unleashed",
                        Owner = new User{ Id = 2, FullName = "User 2", UserName="user2",Password="123456",Role="User"}},
                    new Article { Id = 3, Title = "Silverlight Unleashed",
                        Content = "Short description here", Slug = "Silverlight-Unleashed",
                        Owner = new User{ Id = 3, FullName = "User 3", UserName="user3",Password="123456",Role="User" }}
                };
            // create some mock products to play with
            mockArticleRepository.Setup(ar => ar.GetAll()).Returns(articles);

            mockArticleRepository.Setup(ar => ar.GetById(It.IsAny<int>()))
                                .Returns((int i) => articles.Where(x => x.Id == i).Single());

            mockArticleRepository.Setup(ar => ar.GetArticleBySlug(It.IsAny<string>()))
                                .Returns((string slug) => (from ar in articles
                                                          where ar.Slug == slug
                                                          select ar).FirstOrDefault());

            mockArticleRepository.Setup(ar => ar.GetSlugByArticleId(It.IsAny<int>()))
                                .Returns((int id) => (from ar in articles
                                                      where ar.Id == id
                                                      select ar.Slug).FirstOrDefault());

            mockArticleRepository.Setup(ar => ar.GetAllArticleByUserId(It.IsAny<int>(),It.IsAny<bool>()))
                                .Returns((int id,bool isAdmin) => ( from ar in articles
                                                                    where ar.Owner.Id == id
                                                                    select ar).ToList());           

        }
    }
}