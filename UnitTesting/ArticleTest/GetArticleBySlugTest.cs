﻿using BLL;
using Core.Model;
using System.Collections.Generic;
using Xunit;

namespace UnitTesting.ArticleTest
{
    public class GetArticleBySlugTest
    {
        public IArticleBUS MockArticleRepository;
        private readonly ArticleTest _article = new ArticleTest();
        

        public GetArticleBySlugTest()
        {
            //article = new ArticleTest();
            MockArticleRepository = _article.mockArticleRepository.Object;
        }

        [Fact]
        public void GetArticleBySlug_InvalidSlug_ReturnNull()
        {
            Article testArticle = this.MockArticleRepository.GetArticleBySlug("asd");
            Assert.Null(testArticle);
        }

        [Fact]
        public void GetArticleBySlug_EmptySlug_ReturnNull()
        {
            Article testArticle = this.MockArticleRepository.GetArticleBySlug("");
            Assert.Null(testArticle);
        }

        [Fact]
        public void GetArticleBySlug_CorectSlug_CanReturnArticle()
        {
            Article testArticle = this.MockArticleRepository.GetArticleBySlug("C#-Unleashed");

            Article realArticle = new Article 
            {
                Id = 1,
                Title = "C# Unleashed",
                Content = "Short description here",
                Slug = "C#-Unleashed",
                Owner = new User { Id = 1, FullName = "User 1", UserName = "user1", Password = "123456", Role = "Admin" }
            };

            Assert.NotNull(testArticle);
            Assert.IsType( typeof(Article), testArticle);
            Assert.Equal(testArticle, realArticle);
        }

        [Fact]
        public void CanGetAllArticle()
        {
            IList<Article> testArticle = this.MockArticleRepository.GetAll();           

            Assert.NotNull(testArticle);          
            Assert.Equal(testArticle.Count, 3);
        }
    }
}
