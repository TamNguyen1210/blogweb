﻿using BLL;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTesting.UserTest
{
    class GetUserByUserNameTest
    {
        public IUserBUS MockUserRepository;
        private readonly UserTest _user = new UserTest();        

        public GetUserByUserNameTest()
        {
            MockUserRepository = _user.mockUserRepository.Object;
        }

        [Fact]
        public void GetUserByUserName_InvalidUserName_ReturnNull()
        {
            User result = MockUserRepository.GetUserByUserName("user123");
            Assert.Null(result);
        }

        [Fact]
        public void GetUserByUserName_ValidUserName_ReturnUser()
        {
            User result = MockUserRepository.GetUserByUserName("user2");
            User u = new User { Id = 2, UserName = "user2", Password = "123456", FullName = "Tâm 2", Role = "Admin" };
            Assert.Equal(result, u);
        }
    }
}
