﻿using BLL;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTesting.UserTest
{
    class CheckLoginTest
    {
        public IUserBUS MockUserRepository;
        private readonly UserTest _user = new UserTest();

        public CheckLoginTest()
        {
            MockUserRepository = _user.mockUserRepository.Object;
        }

        [Fact]
        public void CheckLogin_WrongPassword_ReturnNull()
        {
            User result = MockUserRepository.CheckLogin("user1","12345");
            Assert.Null(result);
        }

        [Fact]
        public void CheckLogin_InvalidUserName_ReturnNull()
        {
            User result = MockUserRepository.CheckLogin("user4","123456");
            Assert.Null(result);
        }

        [Fact]
        public void GetUserByUserName_ValidUser_ReturnUser()
        {
            User result = MockUserRepository.CheckLogin("user2","123456");
            User u = new User { Id = 2, UserName = "user2", Password = "123456", FullName = "Tâm 2", Role = "Admin" };
            Assert.Equal(result, u);
        }
    }
}
