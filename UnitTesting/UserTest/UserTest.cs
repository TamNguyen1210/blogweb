﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using Core.Model;
using BLL;

namespace UnitTesting.UserTest
{
    public class UserTest
    {
        public Mock<IUserBUS> mockUserRepository { get; set; }
        public List<User> users { get; set; }
        public UserTest()
        {
            mockUserRepository = new Mock<IUserBUS>();
            users = new List<User>()
                {
                    new User { Id = 1, UserName = "user1", Password = "123456", FullName="Tâm 1", Role = "User"},
                    new User { Id = 2, UserName = "user2", Password = "123456", FullName="Tâm 2", Role = "Admin"},
                    new User { Id = 3, UserName = "user3", Password = "123456", FullName="Tâm 3", Role = "User"}
                };
            // create some mock products to play with
            mockUserRepository.Setup(u => u.GetAll()).Returns(users);

            mockUserRepository.Setup(u => u.GetById(It.IsAny<int>()))
                                .Returns((int i) => users.Where(x => x.Id == i).FirstOrDefault());

            mockUserRepository.Setup(u => u.GetUserByUserName(It.IsAny<string>()))
                                .Returns((string userName) => (from ar in users
                                                           where ar.UserName == userName
                                                               select ar).FirstOrDefault());

            mockUserRepository.Setup(ar => ar.CheckLogin(It.IsAny<string>(), It.IsAny<string>()))
                                .Returns((string userName,string password) => (from u in users
                                                                               where u.UserName == userName 
                                                                                    && u.Password == password
                                                                               select u)
                                                                               .FirstOrDefault());
        
        }
    }
}