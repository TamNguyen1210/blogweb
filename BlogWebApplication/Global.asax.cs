﻿using Autofac;
using Autofac.Integration.Mvc;
using BlogWebApplication.Controllers;
using BlogWebApplication.Helper;
using BlogWebApplication.Models;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace BlogWebApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);           
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);                
                if (authTicket != null && !authTicket.Expired)
                {
                    CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserId;
                    newUser.FullName = serializeModel.FullName;                   
                    newUser.Role = serializeModel.Role;                   
                                
                    HttpContext.Current.User = newUser;
                }
            }
        }

    }
}
