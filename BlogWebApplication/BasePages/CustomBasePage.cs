﻿using Core.Interfaces;
using System.Web.Mvc;

namespace BlogWebApplication.BasePages
{
    public class CustomBasePage : WebViewPage
    {
        
        public IRepository repo { get; set; }
        public IUserRepository userRepo { get; set; }
       
        public override void Execute() { }
    }
}