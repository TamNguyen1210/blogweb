﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebApplication.Helper
{
    public static class StringHelper
    {
        public static string ConvertToLimitString(this string str,int MaxLength)
        {
            if(str.Length > MaxLength)
            {
                return str.Substring(0, MaxLength) + "...";
            }
            return str;
        }
    }
}