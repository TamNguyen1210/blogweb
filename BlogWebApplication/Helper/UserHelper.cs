﻿using BlogWebApplication.Models;
using Core.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebApplication.Helper
{
    public class UserHelper
    {        
                       
        public static bool IsLogged()
        {
            return (HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static int GetCurrentUserId()
        {
            return (HttpContext.Current.User as CustomPrincipal).UserId;
        }

        public static string GetCurrentFullName()
        {
            return (HttpContext.Current.User as CustomPrincipal).FullName;
        }

        public static string GetCurrentUserName()
        {
            return HttpContext.Current.User.Identity.Name;
        }
        
        public static bool IsAdmin()
        {
            return HttpContext.Current.User.IsInRole("Admin");            
        }
        
    }
}