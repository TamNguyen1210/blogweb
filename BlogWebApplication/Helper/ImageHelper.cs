﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace BlogWebApplication.Helper
{
    public static class ImageHelper
    {
        public static byte[] ConvertImageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

       
        public static string ConvertByteArrayToImage(byte[] byteArrayIn)
        {
            var base64 = Convert.ToBase64String(byteArrayIn);
            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            return imgSrc;
        }
        public static void SaveImage(int Id, string pathServer, HttpPostedFileBase img)
        {
            string path = Path.Combine(pathServer, "imgs", Id.ToString());
            Directory.CreateDirectory(path);
            if (img != null)
            {
                img.SaveAs(Path.Combine(path, "main.jpg"));
            }            
        }
        public static void DeleteImage(int Id, string pathServer)
        {
            string path= Path.Combine(pathServer, "imgs", Id.ToString());
            Directory.Delete(path, true);
        }
    }
}