﻿
using BlogWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core.Interfaces;
using Core.Model;
using BlogWebApplication.Helper;

namespace BlogWebApplication.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private IRepository repo;
        public HomeController(IRepository repo)
        {
            this.repo = repo;
        }
        public ActionResult Index()
        {           
            var listArticle = repo.GetAll<Article>().OrderByDescending(a => a.CreatedDate).ToList();
            if (listArticle.Count == 0)
                return RedirectToAction("Login", "Account");
            Article NewestArrticle = listArticle.First();
            listArticle = listArticle.Skip(1).ToList();
            HomePageArticle model = new HomePageArticle() { Articles = listArticle, NewestArticle = NewestArrticle };
            return View(model);
        }
    }
}