﻿
using BlogWebApplication.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;

using Core.Interfaces;
using Core.Model;

using System.Web.Security;
using BlogWebApplication.BasePages;
using BlogWebApplication.Helper;

namespace BlogWebApplication.Controllers
{
    public class AccountController : Controller
    {
        private IRepository repo;
        private IUserRepository UserRepo;
        
        public AccountController(IRepository repo,IUserRepository UserRepo)
        {
            this.repo = repo;
            this.UserRepo = UserRepo;
        }
        [Authorize]
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                User user = UserRepo.CheckLogin(model.UserName, model.Password);
                //User user = UserRepo.CheckLogin(model.UserName, model.Password);
                if (user != null)
                {
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.Id;
                    serializeModel.FullName = user.FullName;                    
                    serializeModel.Role = user.Role;
                    string userData = JsonConvert.SerializeObject(serializeModel);

                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    var authTicket = new FormsAuthenticationTicket(1, user.UserName, DateTime.Now, DateTime.Now.AddMinutes(10), false, userData);
                    string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Response.Cookies.Add(authCookie);
                    if (user.Role.Contains("User"))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    if (user.Role.Contains("Admin"))
                    {
                        return RedirectToAction("Index", "Admin");
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Incorrect username and/or password");
                    return View(model);
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}