﻿using BlogWebApplication.Helper;
using BlogWebApplication.Models;
using Core.Interfaces;
using Core.Model;
using System;
using BLL;
using System.Web;
using System.Web.Mvc;

namespace BlogWebApplication.Controllers
{
    public class ArticleController : Controller
    {
        private IArticleBUS _articleBUS;
        public ArticleController(IArticleBUS articleBUS)
        {
            this._articleBUS = articleBUS;
        }
        // GET: Article
        public ActionResult Index()
        {          
            return View();
        }

        // GET: Article
        public ActionResult Detail(string slug)
        {
            Article article = _articleBUS.GetArticleBySlug(slug);
            ViewBag.RandomArticle = _articleBUS.GetRandomArticleExceptOne(article);
            ViewBag.Comments = _articleBUS.GetAmountCommentByArticleId(article.Id);
            return View(article);
                     
        }

        [ChildActionOnly]
        public PartialViewResult GetReview(int ArticleId)
        {
            ViewBag.Comments = _articleBUS.GetAmountCommentByArticleId(ArticleId);
            return PartialView(_articleBUS.GetAllCommentByArticleId(ArticleId));
           
        }

        [ChildActionOnly]
        public PartialViewResult Review(int ArticleId)
        {
            ReviewViewModel ReviewVM = new ReviewViewModel()
            {
                ArticleId = ArticleId,
                UserId = UserHelper.GetCurrentUserId()
            };
            return PartialView(ReviewVM);           
        }

        [HttpPost]       
        public ActionResult Review(ReviewViewModel review)
        {
            Comment cmt = new Comment()
            {
                ArticleId = review.ArticleId,
                UserId = review.UserId,
                CreatedDate = DateTime.Now,
                Content = review.Content
            };
            //repo.Insert<Comment>(cmt);
            //repo.SaveChanges();
            ViewBag.CommentTotal = _articleBUS.GetAmountCommentByArticleId(review.ArticleId);
            string slug = _articleBUS.GetSlugByArticleId(review.ArticleId);
            return RedirectToAction("Detail",new { slug = slug });
            
        }
        [Authorize]
        public ActionResult NewArticle()
        {
            return View();
        }

        //[Authorize]
        //public ActionResult ListArticle()
        //{
        //   // int UserID = UserRepo.GetCurrentUserIDByUserName(UserHelper.GetCurrentUserName());
        //   // var ListArticle = _articleBUS.GetAllArticleByUserId(UserID, UserHelper.IsAdmin());
        //    return View("ListArticle", ListArticle);
        //}
        
        //[HttpPost]
        //[Authorize]
        //public ActionResult NewArticle(Article article, HttpPostedFileBase file)
        //{             
        //    Article NewArticle = new Article() ;
        //    NewArticle.Title = article.Title;
        //    NewArticle.Slug = article.Slug;
        //    NewArticle.Content = article.Content;
        //    NewArticle.CreatedDate = DateTime.Now;                     
        //    NewArticle.Owner = UserRepo.GetCurrentUserByUserName(UserHelper.GetCurrentUserName());
        //    repo.Insert<Article>(NewArticle);
        //    repo.SaveChanges();
        //    ImageHelper.SaveImage(NewArticle.Id, Server.MapPath("~"), file);
        //    return RedirectToAction("Detail", new { slug = NewArticle.Slug });            
        //}
        //[HttpPost]
        //[Authorize]
        //public ActionResult Remove(int Id)
        //{
        //    repo.Delete<Article>(repo.GetById<Article>(Id));
        //    repo.SaveChanges();
        //    return RedirectToAction("Article");
        //}
    }
}