﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebApplication.Models
{
    public class ArticleViewModel
    {
        public int ArticleID { get; set; }
       
        public string Title { get; set; }
       
        public string Slug { get; set; }
        
        public string Content { get; set; }
        
        public DateTime Date { get; set; }
        public byte[] Thumbnail { get; set; }
        public byte[] Image { get; set; }
        public string Owner { get; set; }
        
    }
}