﻿using Core.Model;
using System.Collections.Generic;

namespace BlogWebApplication.Models
{
    public class HomePageArticle
    {
        public Article NewestArticle { get; set; }
        public List<Article> Articles { get; set; }
    }
}