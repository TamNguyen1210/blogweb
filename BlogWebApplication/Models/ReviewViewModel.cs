﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogWebApplication.Models
{
    public class ReviewViewModel
    {
        public int UserId {get;set;}
        public int ArticleId { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}