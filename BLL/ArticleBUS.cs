﻿using Core.Interfaces;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ArticleBUS : IArticleBUS
    {
        private IArticleRepository _articleRepo;
        private IRepository _repo;
        public ArticleBUS(IArticleRepository articleRepo,IRepository repo) 
        {
            _articleRepo = articleRepo;
            _repo = repo;
        }

        public IList<Article> GetAll()
        {
            return _repo.GetAll<Article>().ToList();
        }

        public IList<Article> GetAllArticleByUserId(int userId, bool isAdmin)
        {
            return _articleRepo.GetAllArticleByUserId(userId, isAdmin);
        }

        public IList<Comment> GetAllCommentByArticleId(int articleId)
        {
            return _articleRepo.GetAllCommentByArticleId(articleId);
        }

        public int GetAmountArticleByUserId(int userId, bool isAdmin)
        {
            return _articleRepo.GetAmountArticleByUserId(userId, isAdmin);
        }

        public int GetAmountCommentByArticleId(int articleId)
        {
            return _articleRepo.GetAmountCommentByArticleId(articleId);
        }

        public Article GetArticleBySlug(string slug)
        {
            return _articleRepo.GetArticleBySlug(slug);
        }

        public Article GetById(int id)
        {
            return _repo.GetById<Article>(id);
        }

        public Article GetRandomArticleExceptOne(Article article)
        {
            return _articleRepo.GetRandomArticleExceptOne(article);
        }

        public string GetSlugByArticleId(int articleId)
        {
            return _articleRepo.GetSlugByArticleId(articleId);
        }       

    }
}
