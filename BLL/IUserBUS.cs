﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IUserBUS 
    {
        IList<User> GetAll();
        User GetById(int id);
        User GetCurrentUserByUserName(string userName);
        User GetUserByUserName(string userName);
        User CheckLogin(string userName, string password);
        int GetAmountArrticleByUserName(string userName);        
    }
}
