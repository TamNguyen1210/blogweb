﻿using Core.Interfaces;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserBUS : IUserBUS
    {
        private IUserRepository _userRepo;
        private IRepository _repo;
        public UserBUS(IUserRepository userRepo,IRepository repo)
        {
            this._userRepo = userRepo;
            _repo = repo;
        }
        public User CheckLogin(string userName, string password)
        {
            return _userRepo.CheckLogin(userName, password);
        }

        public IList<User> GetAll()
        {
            return _repo.GetAll<User>().ToList(); 
        }

        public int GetAmountArrticleByUserName(string userName)
        {
            return _userRepo.GetAmountArrticleByUserName(userName);
        }

        public User GetById(int id)
        {
            return _repo.GetById<User>(id);
        }

        public User GetCurrentUserByUserName(string userName)
        {
            return _userRepo.GetCurrentUserByUserName(userName);
        }       

        public User GetUserByUserName(string userName)
        {
            return _userRepo.GetUserByUserName(userName);
        }
    }
}
