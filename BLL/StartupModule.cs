﻿using Autofac;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class StartupModule : Module
    {
        private string _connString;
        public StartupModule(string connString)
        {
            this._connString = connString;
        }
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule(new DataModule(_connString));
            builder.RegisterType<ArticleBUS>().As<IArticleBUS>();
            builder.RegisterType<UserBUS>().As<IUserBUS>();
            base.Load(builder);
        }
    }
}
