﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IArticleBUS 
    {
        IList<Article> GetAll();
        Article GetById(int id);
        IList<Article> GetAllArticleByUserId(int userId, bool isAdmin);
        Article GetArticleBySlug(string slug);
        IList<Comment> GetAllCommentByArticleId(int articleId);
        string GetSlugByArticleId(int articleId);
        int GetAmountCommentByArticleId(int articleId);
        int GetAmountArticleByUserId(int userId, bool isAdmin);
        Article GetRandomArticleExceptOne(Article article);
    }
}
