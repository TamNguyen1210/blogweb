﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Model;
using Core.Interfaces;


namespace BLL
{
    public class BaseBUS : IBaseBUS
    {
        private readonly IRepository _repo;
        public BaseBUS(IRepository repo)
        {
            this._repo = repo;
        }
        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            _repo.Delete<TEntity>(entity);
            _repo.SaveChanges();
        }

        public IList<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return _repo.GetAll<TEntity>().ToList();
        }

        public TEntity GetById<TEntity>(int id) where TEntity : BaseModel<int>
        {
            return _repo.GetById<TEntity>(id);
        }

        public void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            _repo.Insert<TEntity>(entity);
            _repo.SaveChanges();
        }
    }
}
