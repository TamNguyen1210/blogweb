﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public interface IBaseBUS
    {
        void Delete<TEntity>(TEntity entity) where TEntity : class;
        IList<TEntity> GetAll<TEntity>() where TEntity : class;
        TEntity GetById<TEntity>(int id) where TEntity : BaseModel<int>;
        void Insert<TEntity>(TEntity entity) where TEntity : class;
        //void Update<TEntity>(TEntity entity) where TEntity : class; 
        
    }
}
