﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace DAL
{
    public class Comment
    {
        public Comment()
        {

        }
        
        public int CommentId { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime CreatedDay { get; set; }
        //[ForeignKey("User")]        
        //public int UserID { get; set; }
        public virtual User User { get; set; }
        //[ForeignKey("Article")]
        //public int ArticleID { get; set; }
        public virtual Article Article { get; set; }
    }
}