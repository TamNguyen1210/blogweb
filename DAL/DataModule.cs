﻿using Autofac;
using Core.Interfaces;

namespace DAL
{
    public class DataModule : Module
    {
        private string connStr;
        public DataModule(string connString)
        {
            this.connStr = connString;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new BlogDBContext(this.connStr)).As<IDbContext>().InstancePerRequest();
            builder.RegisterType<SqlRepository>().As<IRepository>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();
            //builder.RegisterType<UserRepository>().As<IRepository>().InstancePerRequest();
            builder.RegisterType<ArticleRepository>().As<IArticleRepository>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();           
            base.Load(builder);
        }
    }
}
