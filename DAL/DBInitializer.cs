﻿using Core.Model;
using System.Data.Entity;


namespace DAL
{
    public class DBInitializer : CreateDatabaseIfNotExists<BlogDBContext>
    {
        protected override void Seed(BlogDBContext context)
        {
            var sqlRep = new SqlRepository(context);
            var user1 = new User {  UserName = "user1", Password = "123456", FullName="Tâm 1", Role = "User"};
            var user2 = new User { UserName = "user2", Password = "123456", FullName = "Tâm 2", Role = "User" };
            var admin = new User { UserName = "admin", Password = "123456", FullName = "Admin", Role = "Admin" };
            sqlRep.Insert<User>(user1);
            sqlRep.Insert<User>(user2);
            sqlRep.Insert<User>(admin);

            context.SaveChanges();

        }
    }
}
