﻿
using Core.Interfaces;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;


namespace DAL
{
    public class BlogDBContext : DbContext, IDbContext
    {
        public BlogDBContext(string connString) : base(connString)
        {
            Database.SetInitializer<BlogDBContext>(new DBInitializer());
            //Database.SetInitializer<BlogDBContext>(null);
            //Database.SetInitializer(new DropCreateDatabaseAlways<BlogDBContext>());
        }
        //public DbSet<User> Users { get; set; }
        //public DbSet<Article> Articles { get; set; }
       // public DbSet<Comment> Comments { get; set; }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
            
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Article>().ToTable("Articles");
            modelBuilder.Entity<Comment>().ToTable("Comments");

            modelBuilder.Entity<User>().HasKey<int>(s => s.Id);
            modelBuilder.Entity<Article>().HasKey<int>(s => s.Id);
            modelBuilder.Entity<Comment>().HasKey<int>(s => s.Id);

            modelBuilder.Entity<Comment>()
            .HasRequired<User>(p => p.User)
            .WithMany()
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
                .HasRequired<Article>(p => p.Article)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Article>()
                .HasRequired<User>(p => p.Owner)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Article>()
                    .HasRequired<User>(s => s.Owner)    // Article entity requires Owner 
                    .WithMany(s => s.Articles);      // User entity includes many Article entities


            modelBuilder.Entity<Comment>()
                    .HasRequired<User>(c => c.User)
                    .WithMany(u => u.Comments);

            modelBuilder.Entity<Comment>()
                    .HasRequired<Article>(c => c.Article)
                    .WithMany(a => a.Comments);
            base.OnModelCreating(modelBuilder);
        }

    }
}