﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var ctx = new BlogDBContext())
            {
                User user = new User() { UserId = 1, FullName = "Nguyễn Trần Tâm", Role="User", Password = "123456", UserName="Tâm" };
                ctx.Users.Add(user);
                ctx.SaveChanges();
            }                      
        }
    }
}
