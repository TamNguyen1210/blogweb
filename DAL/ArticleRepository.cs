﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Model;
using Core.Interfaces;
namespace DAL
{

    public class ArticleRepository : SqlRepository, IArticleRepository
    {
        public ArticleRepository(IDbContext context)
            : base(context)
        {
        }

        public IList<Article> GetAllArticleByUserId(int UserId, bool IsAdmin)
        {
            if(IsAdmin)
            {
                return GetAll<Article>().ToList();
            }
            else
            {
                return GetAll<Article>().Where(a => a.Owner.Id == UserId).ToList();
            }
        }

        public IList<Comment> GetAllCommentByArticleId(int ArticleId)
        {
            return (from c in GetAll<Comment>()
                    where c.ArticleId == ArticleId
                    select c).ToList();
        }

        public int GetAmountArticleByUserId(int UserId, bool IsAdmin)
        {
            if (IsAdmin)
            {
                return GetAll<Article>().Count();
            }
            else
            {
                return GetAll<Article>().Where(a => a.Owner.Id == UserId).Count();
            }
        }

        public int GetAmountCommentByArticleId(int ArticleId)
        {
            return (from c in GetAll<Comment>()
                    where c.ArticleId == ArticleId
                    select c).Count();
        }

        public Article GetArticleBySlug(string Slug)
        {
            return (from a in GetAll<Article>()
                    where a.Slug == Slug select a).FirstOrDefault();
        }

        public Article GetRandomArticleExceptOne(Article _article)
        {
            var listExcept = new List<Article> { _article };
            var rand = new Random();
            var Articles = GetAll<Article>();
            int index = rand.Next(Articles.Count()-1);
            var list = Articles as IList<Article> ?? Articles.ToList();
            Article RandomArticle = list.Except(listExcept).ElementAtOrDefault(index);
            return RandomArticle;
        }

        public string GetSlugByArticleId(int ArticleId)
        {
            return GetById<Article>(ArticleId).Slug;
        }        
    }
}
