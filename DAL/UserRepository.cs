﻿using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Model;

namespace DAL
{
    public class UserRepository : SqlRepository,IUserRepository
    {
        //private IRepository _repository;
        //private readonly IDbContext context;
        public UserRepository(IDbContext context):base(context)
            
        {
            this.context = context;
        }

        public User CheckLogin(string UserName, string Password)
        {
            var User = GetAll<User>().Where(u => u.UserName == UserName && u.Password == Password).FirstOrDefault();
            //var user = (from u in GetAll<User>()
            //where (u.UserName == UserName && u.Password == Password) select u.UserId).FirstOrDefault();
            return User;                          
        }

        public int GetAmountArrticleByUserName(string UserName)
        {
            int UserID = GetAll<User>().Where(u => u.UserName == UserName).Select(u => u.Id).FirstOrDefault();
            return GetAll<Comment>().Where(c => c.User.Id == UserID).Count();
        }
        

        public Core.Model.User GetCurrentUserByUserName(string UserName)
        {
            return this.GetAll<User>().Where(u => u.UserName == UserName).FirstOrDefault();       
        }

        public int GetCurrentUserIDByUserName(string UserName)
        {
            return GetAll<User>().Where(u => u.UserName == UserName).Select(u => u.Id).FirstOrDefault();
        }

        public Core.Model.User GetUserByUserName(string UserName)
        {
            throw new NotImplementedException();
        }
       
    }
}
