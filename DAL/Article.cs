﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL
{
    public class Article
    {        
       
        public int ArticleId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Slug { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public byte[] Thumbnail { get; set; }
        public byte[] Image { get; set; }
        //[ForeignKey("Owner")]
        //public int UserID { get; set; }
        
        public virtual User Owner { get; set; }
        
        public virtual ICollection<Comment> Comments { get; set; }
    }
}